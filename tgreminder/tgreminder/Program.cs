﻿using System;
using Telegram.Bot;
using Telegram.Bot.Extensions;
using Telegram.Bot.Extensions.Polling;
using Telegram.Bot.Types;
using Telegram.Bot.Types.Enums;
using System.Threading;
using System.Threading.Tasks;
using System.Globalization;
using System.Collections.Generic;
using Microsoft.Data.Sqlite;

namespace tgreminder
{
    class Program
    {
        static DB db = new DB();
        static List<Tuple<int, string, string, string, string, int>> items = new List<Tuple<int, string, string, string, string, int>>();
        static List<string> ussers = new List<string>();

        public static void CheckTimeForRem(object data)
        {
            TelegramBotClient botcl = (TelegramBotClient)data;

            var command = db.getConnection().CreateCommand();
            command.CommandText = ("SELECT * FROM Useres");

            db.openConnection();
            using (SqliteDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {
                    ussers.Add((string)reader.GetValue(2));
                }
            }

            db.closeConnection();
            foreach (string s in ussers)
            {
                var command1 = db.getConnection().CreateCommand();
                command1.CommandText = ($"SELECT * FROM '{s}'");

                db.openConnection();
                using (SqliteDataReader reader = command1.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var fir = reader.GetValue(0);
                        var tr = reader.GetValue(1);
                        var fo = reader.GetValue(2);
                        var mes = reader.GetValue(3);
                        var us = reader.GetValue(4);
                        var cha = reader.GetValue(5);
                        items.Add(new Tuple<int, string, string, string, string, int>((int)(long)fir, (string)tr, (string)fo, (string)mes, (string)us, (int)(long)cha));
                    }
                }
                db.closeConnection();
            }
            while (true)
            {
                Thread.Sleep(500);
                foreach (Tuple<int, string, string, string, string, int> s in items)
                {
                    if (s.Item2 + ":00" == DateTime.Now.ToString().Substring(11) && s.Item3.Contains(DateTime.Now.DayOfWeek.ToString()))
                    {
                        Console.WriteLine("Eine Erinnerung wird an den Benutzer gesendet: {0}", s.Item5);
                        botcl.SendTextMessageAsync(s.Item6, $"Erinnerung:\n{s.Item4}");
                    }
                }
            }
        }

        static void Main(string[] args)
        {
            var botClient = new TelegramBotClient("2125884152:AAFXfwQtBhfK0i8FayW5fnnlYE_e5hym8Tc");

            Thread timeChecker = new Thread(new ParameterizedThreadStart(CheckTimeForRem));
            timeChecker.IsBackground = true;
            timeChecker.Start(botClient);

            using var cts = new CancellationTokenSource();
            var receiverOptions = new ReceiverOptions
            {
                AllowedUpdates = { }
            };
            botClient.StartReceiving(
                HandleUpdateAsync,
                HandleErrorAsync,
                receiverOptions,
                cancellationToken: cts.Token
                );

            var me = botClient.GetMeAsync();

            Console.WriteLine($"Start listening for Users");

            Console.WriteLine("Geben Sie den Benutzernamen des Benutzers ein, um ihm den Administratorstatus zu geben");
            string name;
            while (true)
            {
                name = Console.ReadLine();
                try
                {
                    var command = db.getConnection().CreateCommand();
                    command.CommandText = $"UPDATE Useres SET privelegy = 'admin' WHERE username = '{name}'";
                    db.openConnection();
                    command.ExecuteNonQuery();
                    db.closeConnection();
                    Console.WriteLine("Verwaltungsstatus erteilt");
                }
                catch
                {
                    Console.WriteLine("Es gibt keinen solchen Benutzer");
                }
            }
        }

        static string username2;

        static async Task HandleUpdateAsync(ITelegramBotClient botClient, Update update, CancellationToken cancellationToken)
        {
            if (update.Type != UpdateType.Message)
                return;

            if (update.Message!.Type != MessageType.Text)
                return;

            var chatId = update.Message.Chat.Id;
            var username = "@" + update.Message.Chat.Username;
            //Console.WriteLine(username);
            var messageText = update.Message.Text;

            Texter userTexter = new Texter();
            DBChecker dbChecker = new DBChecker(db);

            //Console.WriteLine($"Received a '{messageText}' message in chat {chatId}.");

            if (messageText == "/start")
            {
                if (!dbChecker.tableExists(username))
                {
                    try
                    {
                        Message sentMessage = await botClient.SendTextMessageAsync(
                         chatId: chatId,
                         text: "Sie sind eingeloggt. Um Ihre eigene Erinnerung einzustellen, geben Sie ein:'text';время:**:**;дни:1,2,*,7\nЧтобы поставить всем: 'text';время:**:**;дни:1,2,*,7;пользователи:@***,@****",
                         cancellationToken: cancellationToken);

                        var command = db.getConnection().CreateCommand();
                        command.CommandText = $"INSERT INTO Useres (chatid, username, privelegy, dialog) VALUES ('{chatId}', '{username}', 'user', 'none')";

                        db.openConnection();
                        command.ExecuteNonQuery();
                        db.closeConnection();

                        db.creatNapomList(username);

                        Console.WriteLine("Registrierter Benutzer: {0}", username);
                    }
                    catch(Exception ex)
                    {
                        Console.WriteLine("Registrierung eines Benutzers fehlgeschlagen {0};\n{1}", username,ex.Message);
                    }
                }
                return;
            }


            if (!dbChecker.tableExists(username))
            {
                Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "Schreiben Sie an /start",
                        cancellationToken: cancellationToken);
                return;
            }



            if (userTexter.CheckJson(messageText) && dbChecker.tableExists(username))
            {
                if (userTexter.MsStatus == "user")
                {
                    var command = db.getConnection().CreateCommand();
                    command.CommandText = $"INSERT INTO '{username}' (data, den, word, username, chatId) VALUES ('{userTexter.TimeCheck}', '{userTexter.GetDayOfWeek(userTexter.DniCheck)}', '{userTexter.WordCheck}', '{username}', '{chatId}')";
                    db.openConnection();
                    command.ExecuteNonQuery();
                    db.closeConnection();

                    items.Add(new Tuple<int, string, string, string, string, int>(0, userTexter.TimeCheck, userTexter.GetDayOfWeek(userTexter.DniCheck), userTexter.WordCheck, username, (int)chatId));
                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "Sie haben eine Erinnerung hinzugefügt: \n" + userTexter.WordCheck + "\n" + userTexter.GetDayForMessage(userTexter.DniCheck) + "\n" + userTexter.TimeCheck,
                        cancellationToken: cancellationToken);

                    Console.WriteLine("Zusätzliche Erinnerung an den Benutzer: {0}", username);
                }
                else
                {
                    try
                    {
                        db.closeConnection();
                        if (dbChecker.getPriv(username) == "admin")
                        {
                            string spisok = "";
                            foreach (string us in userTexter.Users)
                            {
                                var command = db.getConnection().CreateCommand();
                                command.CommandText = $"INSERT INTO '{us}' (data, den, word, username, chatId) VALUES ('{userTexter.TimeCheck}', '{userTexter.GetDayOfWeek(userTexter.DniCheck)}', '{userTexter.WordCheck}', '{us}', '{chatId}')";
                                db.openConnection();
                                command.ExecuteNonQuery();
                                db.closeConnection();
                                items.Add(new Tuple<int, string, string, string, string, int>(0, userTexter.TimeCheck, userTexter.GetDayOfWeek(userTexter.DniCheck), userTexter.WordCheck, us, (int)chatId));

                                spisok += ", " + us;
                                Message sentMessagetouser = await botClient.SendTextMessageAsync(
                                    chatId: dbChecker.FindChatIdByUsername(us),
                                    text: "Sie haben eine Erinnerung erhalten: \n" + userTexter.WordCheck + "\n" + userTexter.GetDayForMessage(userTexter.DniCheck) + "\n" + userTexter.TimeCheck,
                                    cancellationToken: cancellationToken);

                                Console.WriteLine("Zusätzliche Erinnerung an den Benutzer: {0}", us);
                            }
                            spisok = spisok.Substring(2);
                            Message sentMessage = await botClient.SendTextMessageAsync(
                                chatId: chatId,
                                text: "Sie haben eine Erinnerung hinzugefügt: \n" + userTexter.WordCheck + "\n" + userTexter.GetDayForMessage(userTexter.DniCheck) + "\n" + userTexter.TimeCheck + " для " + spisok,
                                cancellationToken: cancellationToken);


                        }
                        else
                        {
                            Message sentMessage = await botClient.SendTextMessageAsync(
                                    chatId: chatId,
                                    text: "Sie sind kein Administrator\n",
                                    cancellationToken: cancellationToken);
                        }
                    }
                    catch
                    {
                        Message sentMessage = await botClient.SendTextMessageAsync(
                                    chatId: chatId,
                                    text: "Fehler",
                                    cancellationToken: cancellationToken);
                    }
                }

            }

            if (messageText == "/checkstatus")
            {
                Message sentMessage2 = await botClient.SendTextMessageAsync(
                   chatId: chatId,
                   text: "Ihr Status: " + dbChecker.getPriv(username),
                   cancellationToken: cancellationToken);
            }

            if (messageText == "/userlist")
            {
                Message sentMessage2 = await botClient.SendTextMessageAsync(
                   chatId: chatId,
                   text: username,
                   cancellationToken: cancellationToken);
                var command1 = db.getConnection().CreateCommand();
                command1.CommandText = ($"SELECT * FROM Useres");

                db.openConnection();
                using (SqliteDataReader reader = command1.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var us = reader.GetValue(2);
                        Message sentMessage = await botClient.SendTextMessageAsync(
                   chatId: chatId,
                   text: (string)us,
                   cancellationToken: cancellationToken);
                    }
                }

                db.closeConnection();
            }

            if (messageText == "/setreminder")
            {
                Message sentMessage = await botClient.SendTextMessageAsync(
                     chatId: chatId,
                     text: "'text';Zeit:**:**;Tage:1,2,*,7",
                     cancellationToken: cancellationToken);
            }

            if (messageText == "/myremlist")
            {
                var command1 = db.getConnection().CreateCommand();
                command1.CommandText = ($"SELECT * FROM '{username}'");

                string sio = "";

                db.openConnection();
                using (SqliteDataReader reader = command1.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var fir = reader.GetValue(0);
                        var tr = reader.GetValue(1);
                        var fo = reader.GetValue(2);
                        var mes = reader.GetValue(3);
                        var us = reader.GetValue(4);
                        var cha = reader.GetValue(5);

                        sio += tr + "\t-\t" + fo + "\t-\t" + mes + "\n";
                    }
                }

                db.closeConnection();


                if (sio == "")
                {
                    Message sentMessage1 = await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: "Sie haben keine Erinnerungen",
                    cancellationToken: cancellationToken);
                }
                else
                {
                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: sio,
                        cancellationToken: cancellationToken);
                }
            }

            Der /* Befehl /giveadmin kann verwendet werden, um dem Bot per Nachricht Admin-Rechte zu geben
            if (messageText == "/giveadmin" && getPriv(username) == "creator")
            {
                Message sentMessage = await botClient.SendTextMessageAsync(
                     chatId: chatId,
                     text: "Geben Sie den Benutzernamen des Benutzers ein",
                     cancellationToken: cancellationToken);
                setDialog(username, "getadmin");
                return;
            }

            if (getDialog(username) == "getadmin")
            {
                try
                {
                    var command = db.getConnection().CreateCommand();
                    command.CommandText = $"UPDATE Useres SET privelegy = 'admin' WHERE username = '{messageText}'";
                    db.openConnection();
                    command.ExecuteNonQuery();
                    db.closeConnection();
                }
                catch
                {
                    Message sentMessage = await botClient.SendTextMessageAsync(
                     chatId: chatId,
                     text: "Fehler",
                     cancellationToken: cancellationToken);
                }
            }
            */
            if (messageText == "/remlist" && dbChecker.getPriv(username) == "admin")
            {
                Message sentMessage = await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: "Geben Sie den Benutzernamen des Benutzers ein",
                    cancellationToken: cancellationToken);
                dbChecker.setDialog(username, "getspisokreminder");
                return;
            }

            if (dbChecker.getDialog(username) == "getspisokreminder")
            {
                try
                {
                    var command1 = db.getConnection().CreateCommand();
                    command1.CommandText = ($"SELECT * FROM '{messageText}'");

                    string sio = "";

                    db.openConnection();
                    using (SqliteDataReader reader = command1.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var fir = reader.GetValue(0);
                            var tr = reader.GetValue(1);
                            var fo = reader.GetValue(2);
                            var mes = reader.GetValue(3);
                            var us = reader.GetValue(4);
                            var cha = reader.GetValue(5);

                            sio += tr + "\t-\t" + fo + "\t-\t" + mes + "\n";
                        }
                    }

                    db.closeConnection();


                    if (sio == "")
                    {
                        Message sentMessage1 = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "er hat keine Erinnerungen",
                        cancellationToken: cancellationToken);
                    }
                    else
                    {
                        Message sentMessage = await botClient.SendTextMessageAsync(
                            chatId: chatId,
                            text: sio,
                            cancellationToken: cancellationToken);
                    }
                    dbChecker.setDialog(username, "none");
                }
                catch
                {
                    Message sentMessage1 = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "Fehler!",
                        cancellationToken: cancellationToken);
                    dbChecker.setDialog(username, "none");
                }
            }

            if (messageText == "/deleteall")
            {
                for (int j = 0; j < 10; j++)
                {
                    for (int i = 0; i < items.Count; i++)
                    {
                        if (items[i].Item5 == username)
                        {

                            items.RemoveAt(i);
                        }
                    }
                }

                var command = db.getConnection().CreateCommand();
                command.CommandText = $"DELETE FROM '{username}' WHERE chatid = '{chatId}'";
                db.openConnection();
                command.ExecuteNonQuery();
                db.closeConnection();

                Message sentMessage = await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: "Ваши напоминания удалились",
                    cancellationToken: cancellationToken);

                Console.WriteLine("Удалены все напоминания у пользователя: {0}", username);

            }

            if (messageText == "/delete")
            {
                var command1 = db.getConnection().CreateCommand();
                command1.CommandText = ($"SELECT * FROM '{username}'");
                string sio = " ";
                db.openConnection();
                using (SqliteDataReader reader = command1.ExecuteReader())
                {
                    while (reader.Read())
                    {
                        var fir = reader.GetValue(0);
                        var tr = reader.GetValue(1);
                        var fo = reader.GetValue(2);
                        var mes = reader.GetValue(3);
                        var us = reader.GetValue(4);
                        var cha = reader.GetValue(5);

                        sio += fir + "\t-\t" + tr + "\t-\t" + fo + "\t-\t" + mes + "\n";
                    }
                }
                db.closeConnection();

                Message sentMessage = await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: $"впишите номер напоминания\n{sio}",
                    cancellationToken: cancellationToken);

                dbChecker.setDialog(username, "deleteOneMyRemind");
                return;
            }

            if (dbChecker.getDialog(username) == "deleteOneMyRemind")
            {
                try
                {
                    int del = 0;
                    for (int i = 0; i < items.Count; i++)
                    {
                        if (items[i].Item1 == Convert.ToInt32(messageText) && items[i].Item5 == username)
                        {
                            del = i;
                        }
                    }

                    items.RemoveAt(del);

                    var command = db.getConnection().CreateCommand();
                    command.CommandText = $"DELETE FROM '{username}' WHERE _id = '{messageText}'";

                    db.openConnection();
                    command.ExecuteNonQuery();
                    db.closeConnection();

                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "Ваше напоминание удалилось",
                        cancellationToken: cancellationToken);
                    dbChecker.setDialog(username, "none");

                    Console.WriteLine("Удалено одно напоминание напоминания у пользователя: {0}", username);
                }
                catch
                {
                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "Ошибка",
                        cancellationToken: cancellationToken);
                    dbChecker.setDialog(username, "none");
                }
            }


            if (messageText == "/deleteu" && dbChecker.getPriv(username) == "admin")
            {
                Message sentMessage = await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: $"впишите никнейм юзера",
                    cancellationToken: cancellationToken);


                dbChecker.setDialog(username, "NameForDeleteOneUserRemind");
                return;
            }

            if (dbChecker.getDialog(username) == "NameForDeleteOneUserRemind")
            {
                try
                {
                    username2 = messageText;
                    var command1 = db.getConnection().CreateCommand();
                    command1.CommandText = ($"SELECT * FROM '{messageText}'");
                    string sio = "";
                    db.openConnection();
                    using (SqliteDataReader reader = command1.ExecuteReader())
                    {
                        while (reader.Read())
                        {
                            var fir = reader.GetValue(0);
                            var tr = reader.GetValue(1);
                            var fo = reader.GetValue(2);
                            var mes = reader.GetValue(3);
                            var us = reader.GetValue(4);
                            var cha = reader.GetValue(5);

                            sio += fir + "\t-\t" + tr + "\t-\t" + fo + "\t-\t" + mes + "\n";
                        }
                    }


                    db.closeConnection();

                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: $"впишите номер напоминания\n{sio}",
                        cancellationToken: cancellationToken);


                    dbChecker.setDialog(username, "NumbForDeleteOneUserRemind");
                    return;
                }
                catch
                {
                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: $"ошибка",
                        cancellationToken: cancellationToken);


                    dbChecker.setDialog(username, "none");
                }
            }

            if (dbChecker.getDialog(username) == "NumbForDeleteOneUserRemind")
            {
                try
                {
                    int del = 0;
                    for (int i = 0; i < items.Count; i++)
                    {
                        if (items[i].Item1 == Convert.ToInt32(messageText) && items[i].Item5 == username2)
                        {
                            del = i;
                        }
                    }

                    items.RemoveAt(del);
                    var command = db.getConnection().CreateCommand();
                    command.CommandText = $"DELETE FROM '{username2}' WHERE _id = '{messageText}'";

                    db.openConnection();
                    command.ExecuteNonQuery();
                    db.closeConnection();

                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "это напоминание удалилось",
                        cancellationToken: cancellationToken);

                    Console.WriteLine("Удалено одно напоминание у пользователя: {0}", username2);
                    dbChecker.setDialog(username, "none");
                }
                catch
                {
                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "ошибка!" + username2,
                        cancellationToken: cancellationToken);
                    dbChecker.setDialog(username, "none");
                }
            }

            if (messageText == "/deleteallu" && dbChecker.getPriv(username) == "admin")
            {
                Message sentMessage = await botClient.SendTextMessageAsync(
                    chatId: chatId,
                    text: "впишите username пользователя",
                    cancellationToken: cancellationToken);
                dbChecker.setDialog(username, "deleteAllUserSpisok");
            }

            if (dbChecker.getDialog(username) == "deleteAllUserSpisok")
            {
                try
                {
                    for (int j = 0; j < 10; j++)
                    {
                        for (int i = 0; i < items.Count; i++)
                        {
                            if (items[i].Item5 == messageText)
                            {

                                items.RemoveAt(i);
                            }
                        }
                    }

                    var command = db.getConnection().CreateCommand();
                    command.CommandText = $"DELETE FROM {messageText} WHERE username = '{messageText}'";
                    db.openConnection();
                    command.ExecuteNonQuery();
                    db.closeConnection();

                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "Его напоминания удалились",
                        cancellationToken: cancellationToken);

                    Console.WriteLine("Удалены все напоминания у пользователя: {0}", messageText);
                    dbChecker.setDialog(username, "none");
                }
                catch
                {
                    Message sentMessage = await botClient.SendTextMessageAsync(
                        chatId: chatId,
                        text: "ошибка!",
                        cancellationToken: cancellationToken);
                    dbChecker.setDialog(username, "none");
                }
            }
        }

        static Task HandleErrorAsync(ITelegramBotClient botClient, Exception exception, CancellationToken cancellationToken)
        {
            var ErrorMessage = exception switch
            {
                Telegram.Bot.Exceptions.ApiRequestException apiRequestException
                    => $"Telegram API Error:\n[{apiRequestException.ErrorCode}]\n{apiRequestException.Message}",
                _ => exception.ToString()
            };

            Console.WriteLine(ErrorMessage);
            return Task.CompletedTask;
        }
    }
}
