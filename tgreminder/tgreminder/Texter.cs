﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tgreminder
{
    public class Texter
    {
        public string WordCheck { get; set; }
        public string VremyaCheck { get; set; }
        public string TimeCheck { get; set; }
        public string DayCheck { get; set; }
        public string DniCheck { get; set; }
        public string PolzCheck { get; set; }
        public string UserCheck { get; set; }
        public string[] Users { get; set; }
        public string MsStatus { get; set; }

        public bool CheckJson(string message)
        {
            int semicolonCount = default;
            int colonCount = default;
            for (int i = 0; i < message.Length; i++)
            {
                if (message[i] == ';')
                    semicolonCount++;
                if (message[i] == ':')
                    colonCount++;
            }

            Console.WriteLine("{0} {1}", semicolonCount, colonCount);

            if (semicolonCount == 3 && colonCount == 4 && message.Length>32)
            {
                // Hier wird die Nachricht, die Änderung der Erinnerung im json-Format, in ihre Hauptbestandteile zerlegt
                int fiZa = (message.IndexOf(";")) + 1;
                int leWord = fiZa - 1;
                WordCheck = (message.Substring(0, leWord));

                int fiDv = (message.IndexOf(":"));
                int length = fiDv - fiZa;
                VremyaCheck = (message.Substring(fiZa, length));

                int seZa = (message.IndexOf(";", fiDv));
                int leTime = seZa - fiDv;
                TimeCheck = message.Substring(fiDv + 1, leTime - 1);

                int seDv = (message.IndexOf(":", seZa));
                int leDay = seDv - seZa + 1;
                DayCheck = message.Substring(seZa + 1, leDay - 2);

                int thZa = (message.IndexOf(";", seDv));
                int leDni = thZa - 1 - seDv + 1;
                DniCheck = message.Substring(seDv + 1, leDni - 1);

                int thDv = (message.IndexOf(":", thZa));
                int lePolz = thDv - thZa + 1;
                PolzCheck = message.Substring(thZa + 1, lePolz - 2);

                int leUser = message.Length - 1 - thDv + 1;
                UserCheck = message.Substring(thDv + 1, leUser - 1);

                VremyaCheck = VremyaCheck.Replace(" ", ""); TimeCheck = TimeCheck.Replace(" ", "");
                DayCheck = DayCheck.Replace(" ", "");
                DniCheck = DniCheck.Replace(" ", "");
                PolzCheck = PolzCheck.Replace(" ", "");
                UserCheck = UserCheck.Replace(" ", "");

                if (VremyaCheck.ToLower() != "время")
                    return false;

                if (DayCheck.ToLower() != "дни")
                    return false;

                if (PolzCheck.ToLower() != "пользователи")
                    return false;

                Users = UserCheck.Split(',');
                MsStatus = "admin";

                string pattern = "H:mm";
                DateTime parsedDate;
                if (DateTime.TryParseExact(TimeCheck, pattern, null,
                                        DateTimeStyles.None, out parsedDate))
                {
                    //Console.WriteLine("Converted '{0}' to {1:H:mm}.",
                    //                  message, parsedDate);
                    return true;
                }
                else
                {
                    //Console.WriteLine("Unable to convert '{0}' to a date and time.",
                    //                   message);
                    return false;
                }
            }

            else if (semicolonCount == 2 && colonCount == 3 && message.Length > 17)
            {
                int fiZa = (message.IndexOf(";")) + 1;
                int leWord = fiZa - 1;
                WordCheck = (message.Substring(0, leWord));

                int fiDv = (message.IndexOf(":"));
                int length = fiDv - fiZa;
                if (length <= 0)
                    return false;
                VremyaCheck = (message.Substring(fiZa, length));

                int seZa = (message.IndexOf(";", fiDv));
                int leTime = seZa - fiDv;
                TimeCheck = message.Substring(fiDv + 1, leTime - 1);

                int seDv = (message.IndexOf(":", seZa));
                int leDay = seDv - seZa + 1;
                DayCheck = message.Substring(seZa + 1, leDay - 2);

                int leDni = message.Length - 1 - seDv + 1;
                DniCheck = message.Substring(seDv + 1, leDni - 1);
                VremyaCheck = VremyaCheck.Replace(" ", ""); TimeCheck = TimeCheck.Replace(" ", "");
                DayCheck = DayCheck.Replace(" ", "");
                DniCheck = DniCheck.Replace(" ", "");

                if (VremyaCheck.ToLower() != "время")
                    return false;

                if (DayCheck.ToLower() != "дни")
                    return false;

                //Console.WriteLine($"{wordCheck} {VremyaCheck} {TimeCheck} {DayCheck} {DniCheck}");
                MsStatus = "user";
                string pattern = "H:mm";
                DateTime parsedDate;
                if (DateTime.TryParseExact(TimeCheck, pattern, null,
                                        DateTimeStyles.None, out parsedDate))
                {
                    //Console.WriteLine("Converted '{0}' to {1:H:mm}.",
                    //                  message, parsedDate);
                    return true;
                }
                else
                {
                    //Console.WriteLine("Unable to convert '{0}' to a date and time.",
                    //                  message);
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public string GetDayOfWeek(string message)
        {
            string dni = "";
            if (message.Contains('1'))
            {
                dni = dni + ", Monday";
            }
            if (message.Contains('2'))
            {
                dni = dni + ", Tuesday";
            }
            if (message.Contains('3'))
            {
                dni = dni + ", Wednesday";
            }
            if (message.Contains('4'))
            {
                dni = dni + ", Thursday";
            }
            if (message.Contains('5'))
            {
                dni = dni + ", Friday";
            }
            if (message.Contains('6'))
            {
                dni = dni + ", Saturday";
            }
            if (message.Contains('7'))
            {
                dni = dni + ", Sunday";
            }
            return dni.Substring(1);

        }

        public string GetDayForMessage(string message)
        {
            string dni = "";
            if (message.Contains('1'))
            {
                dni = dni + ", Montag";
            }
            if (message.Contains('2'))
            {
                dni = dni + ", Dienstag";
            }
            if (message.Contains('3'))
            {
                dni = dni + ", Mittwoch";
            }
            if (message.Contains('4'))
            {
                dni = dni + ", Donnerstag";
            }
            if (message.Contains('5'))
            {
                dni = dni + ", Freitag";
            }
            if (message.Contains('6'))
            {
                dni = dni + ", Samstag";
            }
            if (message.Contains('7'))
            {
                dni = dni + ", Sonntag";
            }
            return dni.Substring(1);
        }
    }
}
