﻿using Microsoft.Data.Sqlite;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace tgreminder
{
    class DBChecker
    {
        DB db;
        public DBChecker(DB _db)
        {
            db = _db;
        }

        public void setDialog(string username, string dialogSt)
        {
            var command = db.getConnection().CreateCommand();
            command.CommandText = $"UPDATE Useres SET dialog = '{dialogSt}' WHERE username = '{username}'";
            db.openConnection();
            command.ExecuteNonQuery();
            db.closeConnection();
        }

        public string getDialog(string username)
        {
            var command2 = db.getConnection().CreateCommand();
            command2.CommandText = $"SELECT dialog FROM Useres WHERE username = '{username}'";
            db.openConnection();
            string stat = "user";
            using (SqliteDataReader reader = command2.ExecuteReader())
            {
                while (reader.Read())
                {
                    stat = (string)reader.GetValue(0);
                }
            }

            db.closeConnection();

            return stat;
        }

        public string getPriv(string username)
        {
            var command2 = db.getConnection().CreateCommand();
            command2.CommandText = $"SELECT privelegy FROM Useres WHERE username = '{username}'";
            db.openConnection();
            string stat = "user";
            using (SqliteDataReader reader = command2.ExecuteReader())
            {
                while (reader.Read())
                {
                    stat = (string)reader.GetValue(0);
                }
            }

            db.closeConnection();
            return stat;
        }

        public int FindChatIdByUsername(string username)
        {
            int result = 0;
            var command = db.getConnection().CreateCommand();
            command.CommandText = ("SELECT * FROM Useres");
            db.openConnection();
            using (SqliteDataReader reader = command.ExecuteReader())
            {
                while (reader.Read())
                {

                    var cha = reader.GetValue(1);
                    var us = reader.GetValue(2);
                    if ((string)us == username)
                        result = (int)(long)cha;
                }
            }
            db.closeConnection();
            return result;
        }

        public bool tableExists(string usforCheck)
        {
            string sqlExpression = $"SELECT * FROM Useres";
            string usernamesOnDB = "";
            using (db.getConnection())
            {
                db.openConnection();

                SqliteCommand command = new SqliteCommand(sqlExpression, db.getConnection());
                using (SqliteDataReader reader = command.ExecuteReader())
                {
                    if (reader.HasRows) // если есть данные
                    {
                        while (reader.Read())   // построчно считываем данные
                        {
                            usernamesOnDB += reader.GetValue(2);
                        }

                    }
                }
                db.closeConnection();

            }
            if (usernamesOnDB.Contains(usforCheck))
            {
                return true;
            }
            return false;

        }
    }
}
